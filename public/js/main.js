const parallax = document.querySelector(".parallax");
const loveContainer = document.querySelector(".love-container");
const body = document.querySelector("body");
const btns = document.querySelectorAll(".btn");

btns.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        console.log("dw")
        e.preventDefault();
        parallax.classList.toggle("active")
        loveContainer.classList.toggle("active")
        body.classList.toggle("active")
    });
});
